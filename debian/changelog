puppet-module-octavia (25.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Oct 2024 11:22:31 +0200

puppet-module-octavia (24.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Apr 2024 17:14:53 +0200

puppet-module-octavia (23.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed versions of depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Oct 2023 13:42:00 +0200

puppet-module-octavia (22.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Jun 2023 11:26:55 +0200

puppet-module-octavia (22.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed runtime depends versions for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 12 Apr 2023 11:12:17 +0200

puppet-module-octavia (21.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 20 Oct 2022 23:29:39 +0200

puppet-module-octavia (20.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2022 14:35:14 +0200

puppet-module-octavia (20.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 14:05:50 +0100

puppet-module-octavia (20.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Mar 2022 23:04:43 +0100

puppet-module-octavia (19.4.0-2) unstable; urgency=medium

  * Clean-up update-alternatives handling.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Jan 2022 10:19:37 +0100

puppet-module-octavia (19.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Oct 2021 11:03:19 +0200

puppet-module-octavia (19.3.0-1) unstable; urgency=medium

  * Uploading to unstable.
  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 15:09:43 +0200

puppet-module-octavia (19.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.
  * Removed Add-support-for-octavia_api_uwsgi_config-in-Debian.patch applied
    upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Sep 2021 15:28:53 +0200

puppet-module-octavia (18.4.0-2) unstable; urgency=medium

  * Add Add-support-for-octavia_api_uwsgi_config-in-Debian.patch.
  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 17:29:43 +0200

puppet-module-octavia (18.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 06 Apr 2021 13:25:55 +0200

puppet-module-octavia (18.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 22:05:51 +0100

puppet-module-octavia (18.2.0-1) experimental; urgency=medium

  * Fixed debian/watch.
  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 22 Mar 2021 20:56:59 +0100

puppet-module-octavia (17.4.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 16:31:49 +0200

puppet-module-octavia (17.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Oct 2020 14:06:03 +0200

puppet-module-octavia (16.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 11:46:24 +0200

puppet-module-octavia (16.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 03 May 2020 13:56:19 +0200

puppet-module-octavia (16.2.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 26 Apr 2020 12:43:02 +0200

puppet-module-octavia (15.4.0-3) unstable; urgency=medium

  * Depends on puppet, not puppet-common.
  * Standards-Version: 4.5.0.
  * debhelper-compat (= 11).

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Mar 2020 10:59:18 +0100

puppet-module-octavia (15.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Oct 2019 00:32:13 +0200

puppet-module-octavia (15.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Oct 2019 11:06:58 +0200

puppet-module-octavia (15.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Sep 2019 11:45:15 +0200

puppet-module-octavia (14.4.0-3) unstable; urgency=medium

  * Switched build-dependencies to Python 3 (Closes: #937365).

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Sep 2019 21:14:25 +0200

puppet-module-octavia (14.4.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Jul 2019 22:02:56 +0200

puppet-module-octavia (14.4.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed runtime depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 09 Apr 2019 10:51:22 +0200

puppet-module-octavia (13.1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #916422)

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Nov 2018 16:58:31 +0100
